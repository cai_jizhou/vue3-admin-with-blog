/*
 * @Author: 蔡继周
 * @Date: 2021-11-09 21:34:28
 * @LastEditTime : 2022-02-08 11:06:12
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\B\\vue3-admin-with-blog\\vite.config.js
 */
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// https://github.com/vitejs/vite/tree/main/packages/plugin-legacy
import legacy from "@vitejs/plugin-legacy";


const path = require('path')
import { dependencies } from './package.json';
// https://vitejs.dev/config/
export default defineConfig({
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './src')
        }
    },
    server: {
        host: '0.0.0.0',
        port:8888,
        proxy: {
            "/api": {
                target: "http://localhost:3000",
                rewrite: path => path.replace(/^\/api/, ''),
                changeOrigin: true,
            }
        },
        
    },
    base: "/admin/",
    build: {
        target: "es2015", // 默认 "modules"
        outDir:__dirname+ '/../koa2-admin-with-blog/admin', //配置打包文件生成路径
        //生产环境移除 console
        terserOptions: {
            compress: {
            //   drop_console: true,
            //   drop_debugger: true
            }
        },
        emptyOutDir: true,
        rollupOptions: {
            output:{
                chunkFileNames: "static/js/[name]-[hash].js",
                entryFileNames: "static/js/[name]-[hash].js",
                assetFileNames: "static/[ext]/[name]-[hash].[ext]",
            }
        }
    },
    css: {
        preprocessorOptions: {
          scss: {
            additionalData: '@import "./src/styles/common.scss";'
          }
        }
      },
    plugins: [
        vue(),
        legacy({
        targets: ["ie >= 11"],
        additionalLegacyPolyfills: ["regenerator-runtime/runtime"],
        })
    ]
})