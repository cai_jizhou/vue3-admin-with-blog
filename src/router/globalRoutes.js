/*
 * @Author: your name
 * @Date: 2021-01-13 17:37:21
 * @LastEditTime : 2021-12-23 11:24:21
 * @LastEditors  : Shaquille.Cai
 * @Description: In User Settings Edit
 * @FilePath     : \\vue3-admin-with-blog\\src\\router\\globalRoutes.js
 */
export default [{
        name: 'login',
        path: '/login',
        meta: {
            title: '登录'
        },
        component: () =>
            import ('@/views/Login.vue')
},
{
    name: 'refresh',
    path: '/refresh',
    meta: {
        title: '刷新'
    },
    component: () =>
        import ('@/views/Refresh.vue')
},
{
    name: '404',
    path: '/404',
    meta: {
        title: '页面不存在'
    },
    component: () =>
        import ('@/views/404.vue')
}
];