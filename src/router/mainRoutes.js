/*
 * @Author: your name
 * @Date: 2021-01-13 17:39:02
 * @LastEditTime : 2021-12-24 17:13:56
 * @LastEditors  : Shaquille.Cai
 * @Description: In User Settings Edit
 * @FilePath     : \\vue3-admin-with-blog\\src\\router\\mainRoutes.js
 */
export default {
    path: "/",
    name: "AppMain",
    component: () =>
        import ("@/views/AppMain.vue"),
    redirect: { name: "Welcome" },
    meta: {
        title: '首页'
    },
    children: [{
            name: 'Welcome',
            path: '/welcome',
            meta: {
                title: 'welcome'
            },
            component: () =>
                import ('@/views/Welcome.vue')
        }]
}