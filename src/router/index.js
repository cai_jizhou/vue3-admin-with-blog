/*
 * @Author: 蔡继周
 * @Date: 2021-11-09 21:39:56
 * @LastEditTime : 2021-12-20 10:23:03
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\vue3-admin-with-blog\\src\\router\\index.js
 */
import { createRouter, createWebHashHistory } from 'vue-router'
import globalRoutes from './globalRoutes'
import mainRoutes from './mainRoutes'

const router = createRouter({
    history: createWebHashHistory(),
    scrollBehavior: () => ({ y: 0 }),
    isAddDynamicMenuRoutes: false, // 是否已经添加动态(菜单)路由
    routes: globalRoutes.concat(mainRoutes)
})
export default router