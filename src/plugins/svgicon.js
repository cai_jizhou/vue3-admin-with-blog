/*
 * @Description  : 
 * @Author       : CaiJiZhou
 * @Date         : 2021-12-14 11:11:42
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-20 23:17:32
 * @FilePath     : \\myBlog\\blogAdmin\\src\\plugins\\svgicon.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */

import * as components from "@element-plus/icons";
export default {
    install: (app) => {
        //遍历所有的icon名称进行注册
        for (const key in components) {
            const componentConfig = components[key];
            app.component(componentConfig.name, componentConfig);
        }
    },
};