/*
 * @Description  : 
 * @Author       : CaiJiZhou
 * @Date         : 2021-12-14 11:12:07
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2022-01-10 17:05:12
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\plugins\\permission.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */
import mainRoutes from "@/router/mainRoutes";


/**
 * @description: 将路由信息列表转化为可渲染的路由对象            //待优化，应获取多层级文件
 * @param {asyncRouterMap} 路由信息列表
 * @return {*}
 */
const filterAsyncRouter = (asyncRouterMap) => {
    const modules = getModules();
    const components = getComponents();
    const allFileList=[]
    Object.keys(modules).forEach(key => {
        const viewSrc = components[key];
        const file = viewSrc.default;
        allFileList.push({
            name: `${file.name}`,
            component: modules[key]
          })
    })
    const accessedRouters = [...asyncRouterMap]
    allFileList.forEach(v => {
        accessedRouters.forEach(route => {
            if (v.name===route.name) {
                route.component=v.component
            }
        })
    })
    return accessedRouters
}

/**
 * @description: 获取views中所有文件中以.vue结尾的文件
 * @param {*}
 * @return {*}
 */
function getModules() {
    //大爷的，vite的路径不能用变量，文件夹能套几十层，我直接G
    const components = import.meta.glob('../views/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/*.vue')
    return components
}
  /**
   * @description: 获取views中所有文件中以.vue结尾的文件（区别见vite文档）
   * @param {*}
   * @return {*}
   */
  function getComponents() {
    const components = import.meta.globEager('../views/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/**/*.vue')
    return components
  }


export default {
    install: (app, { router, store }) => {
        router.beforeEach(async (to, from, next) => {
                //修改标题
                document.title = to.meta.title || '';
                //是否有token
                const hasToken = !!store.getters['moduleInfo/token']
                if (hasToken) {
                    //有token去login则定位到welcome
                    if (to.path === '/login') {
                        next({
                            path: '/welcome'
                        })
                    } else {
                        const hasGetUserInfo = store.getters['moduleInfo/userInfo']?.userName
                        //有token，是否有用户信息（这一步基本上只用于，登录获取token后，获取用户信息的操作），并且是否已经加载完动态路由
                        if (hasGetUserInfo && router.options.isAddDynamicMenuRoutes) {
                            const hasPath=router.getRoutes().findIndex(v => v.path === to.path)!==-1
                            hasPath ? next() :next({
                                path: '/404'
                            })
                        } else {
                            try {
                                console.info('有token并且去的不是登录页,但是没有用户信息亦或是初始加载~')
                                //获取用户信息
                                await store.dispatch('moduleInfo/getUserInfo')
                                //获取路由信息
                                const menuResult = await store.dispatch('moduleInfo/setMenuRes')
                                //转化路由列表
                                const data = filterAsyncRouter(menuResult)
                                //并入 主路由的children中
                                mainRoutes['children'] = [...mainRoutes['children'], ...data]
                                await store.dispatch('moduleInfo/setRouterRes', mainRoutes['children'])
                                console.log(mainRoutes)
                                router.addRoute(mainRoutes)
                                //更改为已经加载完动态路由
                                router.options.isAddDynamicMenuRoutes=true
                                next({
                                    ...to,
                                    replace: true
                                })
                            } catch (error) {
                                // 这一步出问题直接清除所有信息回到login
                                await store.dispatch('moduleInfo/resetToken') //触发  resetToken
                                next()
                            }
                        }
                    }
            } else if (!hasToken && to.path === '/login') {
                // 如果没有token，并且去的路由是登录，继续走
                next()
            } else if (!hasToken && to.path !== '/login') {
                // 如果没有token，并且去的路由是其他页面，回到登录
                next({ path: '/login'})
            }
        })
    }
}