/*
 * @Description  : 
 * @Author       : CaiJiZhou
 * @Date         : 2021-12-23 10:41:24
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2022-02-08 16:46:40
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\components\\index.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */
export default {
    install: (app) => {
        // ItemView子文件加载
        const modulesChild = import.meta.glob('./FormTep/ItemView/*.vue');
        const componentsChild = import.meta.globEager('./FormTep/ItemView/*.vue');
        const allFileListChild=[]
        Object.keys(modulesChild).forEach(key => {
            const viewSrc = componentsChild[key];
            const file = viewSrc.default;
            allFileListChild.push({
                name: file.name,
                component: file
            })
        })
        allFileListChild.forEach(v => {
            app.component(v.name, v.component);
        })


        const modules = import.meta.glob('./*/*.vue');
        const components = import.meta.globEager('./*/*.vue');
        const allFileList=[]
        Object.keys(modules).forEach(key => {
            const viewSrc = components[key];
            const file = viewSrc.default;
            allFileList.push({
                name: file.name,
                component: file
            })
        })
        allFileList.forEach(v => {
            app.component(v.name, v.component);
        })
    },
};
