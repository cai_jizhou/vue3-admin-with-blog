/*
 * @Author: 蔡继周
 * @Date: 2021-11-17 15:40:00
 * @LastEditTime: 2021-11-17 15:40:00
 * @LastEditors: 蔡继周
 * @Description: I have to do something...
 * @FilePath: \TableTep\src\components\TableTep\mock.js
 */



const dataList = ({ limit, page, ...prop }) => {
    let res = []
    for (let index = 0; index < 19; index++) {
        res.push({
            date: "2016-05-03",
            name: "Tom" + index,
            address: "No. 189, Grove St, Los AngelesNo. 189, Grove St, Los AngelesNo. 189, Grove St, Los AngelesNo. 189, Grove St, Los AngelesNo. 189, Grove St, Los Angeles",
            id: index + 1,
        })
    }
    const start = (page - 1) * limit + 1
    const end = page * limit + 1
    let dataCopy = [...res]
    return dataCopy.slice(start, end)
}
export {
    dataList
}