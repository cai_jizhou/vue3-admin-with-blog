/*
 * @Author: 蔡继周
 * @Date: 2021-11-16 11:24:37
 * @LastEditTime: 2021-11-16 11:24:37
 * @LastEditors: 蔡继周
 * @Description: I have to do something...
 * @FilePath: \TableTep\src\components\FormTep\hooks\computedData.js
 */

//useCounter.js
import { ref } from "vue";

export default (props) => {
    const { column } = props;
    const rules = ref([]);
    const placeholder = column.placeholder ?
        column.placeholder :
        `请输入${column.title}`;
    rules.value = [{
        required: column.required,
        message: placeholder,
        trigger: column.trigger,
    }, ];
    if (column && column.rules) {
        rules.value = [...rules.value, ...column.rules];
    }
    return {
        rules
    }
}