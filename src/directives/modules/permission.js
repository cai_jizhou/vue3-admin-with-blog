/*
 * @Description  : 
 * @Author       : CaiJiZhou
 * @Date         : 2021-12-22 09:47:03
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2022-02-08 10:59:45
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\directives\\modules\\permission.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */
const permission = (app,router,store ,name) => {
    // app.directive(name, {
    //     // 指令 生命周期hooks :
    //     // 在装入绑定元素的父组件之前调用
    //     beforeMount() {
           
    //     },
    //     // 在装入绑定元素的父组件时调用
    //     mounted(el, vNode) {
    //         const permissionList = router.currentRoute.value.meta?.action.map(v=>v.menuCode)
    //         const value = vNode?.value
           
    //         if (value && value instanceof Array && value.length > 0) {
    //             const hasPermission = permissionList.some(permission => {
    //                 return value.includes(permission);
    //             });
    //             if (!hasPermission) {
    //                 el.parentNode && el.parentNode.removeChild(el)
    //               }
    //         } else {
    //             throw new Error(`需要的权限， v-permission="['权限code1','权限code2','权限code3','权限code4']"`)
    //         }
    //     },
    //     // 在更新包含组件的VNode之前调用
    //     beforeUpdate() {},
    //     // 在包含组件的VNode及其子节点的VNode//更新后调用
    //     updated() {},
    //     // 在卸载绑定元素的父组件之前调用
    //     beforeUnmount() {},
    //     // 在卸载绑定元素的父组件时调用
    //     unmounted() {}
    // })
}
export default permission
