/*
 * @Description  : 
 * @Author       : CaiJiZhou
 * @Date         : 2021-12-22 09:47:35
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-22 10:41:16
 * @FilePath     : \\vue3-admin-with-blog\\src\\directives\\index.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */

function getModules() {
    const components = import.meta.glob('./modules/*.js')
    return components
}
 
function getComponents() {
    const components = import.meta.globEager('./modules/*.js')
    return components
}

export default {
    install: (app, { router, store }) => {
        const modules = getModules();
        const components = getComponents();
        const allFileList=[]
        Object.keys(modules).forEach(key => {
            const viewSrc = components[key];
            const file = viewSrc.default;
            allFileList.push({
                name: file.name,
                method: file
            })
        })
        allFileList.forEach(v => {
            v.method(app,router,store ,v.name)
        })
    },
};

