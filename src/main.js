/*
 * @Author: 蔡继周
 * @Date: 2021-11-09 21:34:28
 * @LastEditTime : 2022-02-08 10:41:14
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\main.js
 */
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import ElementIcon from "@/plugins/svgicon";
import permission from "@/plugins/permission"; //权限控制
import directives from "@/directives";         //自定义指令
import allComponents from './components/index';//所有组件加载

const app = createApp(App);
import 'default-passive-events'

app.use(router)
    .use(store)
    .use(ElementPlus, { size: 'small', locale: zhCn })
    .use(ElementIcon)
    .use(permission, { router, store })
    .use(allComponents)
    .use(directives, { router, store })
    .mount('#app')
