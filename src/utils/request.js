/*
 * @Description  : axios二次封装
 * @Author       : CaiJiZhou
 * @Date         : 2021-11-18 09:50:08
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-24 17:17:31
 * @FilePath     : \\vue3-admin-with-blog\\src\\utils\\request.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */

import axios from 'axios'
import config from './../config'
import { ElMessage } from 'element-plus'
import router from './../router'
import store from './../store'
import storage from './storage'
import { showFullScreenLoading, hideFullScreenLoading } from './loadingContent'

const TOKEN_INVALID = 'Token认证失败，请重新登录'
const NETWORK_ERROR = '网络请求异常，请稍后重试'
    // 创建axios实例对象，添加全局配置
const service = axios.create({
    baseURL: config.baseApi,
    timeout: 8000
})

// 请求拦截
service.interceptors.request.use((req) => {
    const headers = req.headers;
    const token = storage.getItem('token') || {};
    if (!headers.Authorization) headers.Authorization = 'Bearer ' + token;
    //对loading的dom进行选择
    req.loadDom ? showFullScreenLoading(req.loadDom) : showFullScreenLoading()
    return req;
})

// 响应拦截
service.interceptors.response.use((res) => {
        const { code, data, msg } = res.data;
        hideFullScreenLoading()
        if (code === 200) {
            return data;
        } else if (code === 500001) {
            ElMessage.error(TOKEN_INVALID)
            setTimeout(() => {
                store.dispatch("moduleInfo/resetToken").then(() => {
                    router.push('/login')
                  });
            }, 500)
            return Promise.reject(TOKEN_INVALID)
        } else {
            ElMessage.error(msg || NETWORK_ERROR)
            return Promise.reject(msg || NETWORK_ERROR)
        }
    })
    /**
     * 请求核心函数
     * @param {*} options 请求配置
     */
function request(options) {
    options.method = options.method || 'get'
    if (options.method.toLowerCase() === 'get') {
        options.params = options.data || options.params
    }
    let isMock = config.mock;
    if (typeof options.mock != 'undefined') {
        isMock = options.mock;
    }
    if (config.env === 'prod') {
        service.defaults.baseURL = config.baseApi
    } else {
        service.defaults.baseURL = isMock ? config.mockApi : config.baseApi
    }
    return service(options)
}


export default request;