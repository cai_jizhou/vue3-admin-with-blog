/*
 * @Description  : 储存
 * @Author       : CaiJiZhou
 * @Date         : 2021-11-18 09:50:08
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-29 14:51:24
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\utils\\storage.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */

import config from './../config'
//请勿储存函数
export default {
    setItem(key, val) {
        let storage = this.getStroage();
        storage[key] = val;
        window.localStorage.setItem(config.namespace, JSON.stringify(storage));
    },
    getItem(key) {
        return this.getStroage()[key]
    },
    getStroage() {
        return JSON.parse(window.localStorage.getItem(config.namespace) || "{}");
    },
    clearItem(key) {
        let storage = this.getStroage()
        delete storage[key]
        window.localStorage.setItem(config.namespace, JSON.stringify(storage));
    },
    clearAll() {
        window.localStorage.clear()
    }
}