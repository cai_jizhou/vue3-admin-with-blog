/*
 * @Description  : 工具
 * @Author       : CaiJiZhou
 * @Date         : 2021-11-18 09:50:08
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-29 13:25:06
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\utils\\utils.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */
/**
 * @description:  置空一个对象
 * @param {*}
 * @return {*}
 */
const clearObj = (object) => {
        const obj = {...object }
        Object.keys(obj).forEach(key => {
            if (Array.isArray(obj[key])) {
                obj[key] = []
            }
            if (typeof obj[key] === 'number' && !isNaN(obj[key])) {
                obj[key] = null
            }
            if (typeof(obj[key]) === 'string') {
                obj[key] = null
            }
            if (Object.prototype.toString.call(obj[key]) === '[object Object]') {
                obj[key] = clearObj(obj[key])
            }
            if (typeof(obj[key]) === 'boolean') {
                obj[key] = false
            }
        })
        return obj
    }
/**
 * @description: 扁平化数据转为树形结构
 * @param {*}
 * @return {*}
 */    
const FlatToTree = (arr, pidStr = 'parent_id', idStr = 'id', childrenStr = 'children') => {
    let temp = {} //扁平数组转对象 key是id  value是数组中id所对应的对象
    let tree = [] //最终数组
    for (let index = 0; index < arr.length; index++) {
        temp[arr[index][idStr]] = arr[index]
    }
    for (let key = 0; key < arr.length; key++) {
        //temp中 是否有对应的父级数据
        const parentNode = temp[arr[key][pidStr]]
        if (parentNode) {
            // 如果有父级数据，则在父级数据中建立children字段
            !parentNode[childrenStr] && (parentNode[childrenStr] = [])
                //  将当前数据插入到父级数据的children中            
            parentNode[childrenStr].push(arr[key])
        } else {
            // 如果没有直接插入
            tree.push(arr[key])
        }
    }
    return tree
}

/**
 * @description: 树形结构转为扁平化数据
 * @param {*}
 * @return {*}
 */
const TreeToFlat = (arr)=>{
    let res = []
    arr.forEach(el=>{
        res.push(el)
        el.children && res.push(...TreeToFlat(el.children))
    })
    return res
}

/**
 * @description: 树形结构找到对应的数据
 * @param {*}
 * @return {*}
 */
const treeFindMany =(key='_id',value, data, res)=> {
    if (res == undefined) {
        var res = new Array();
    }
    for (let j in data) {
        if (data[j][key]===value) {
            res.push(data[j])
        }
        if (data[j].children !== undefined) {
            treeFindMany(key,value, data[j].children, res)
        }
    }
    return res
}    

/**
 * @description: 扁平数据找到所有有关联的数据
 * @param {*}
 * @return {*}
 */
const flatAboutMany =(key='parentId',value, data, res)=> {
    if (res == undefined) {
        var res = new Array();
    }
    for (let j in data) {
        if (data[j]['_id'] === value) {
            res.unshift(data[j])
            if (data[j][key]) {
                flatAboutMany(key,data[j][key],data,res)
            }
        }
    }
    return res
}   


const resetZero = (data) => {
    if (data===0) {
        return true
    }
    return !!data
}

export {
    clearObj,
    FlatToTree,
    treeFindMany,
    resetZero,
    TreeToFlat,
    flatAboutMany
}