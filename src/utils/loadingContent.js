/*
 * @Description  : loading框设置局部刷新，且所有请求完成后关闭loading框
 * @Author       : CaiJiZhou
 * @Date         : 2021-11-18 09:50:08
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-23 15:35:28
 * @FilePath     : \\vue3-admin-with-blog\\src\\utils\\loadingContent.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */

import { ElLoading } from 'element-plus'

let loading
let needLoadingRequestCount = 0 // 声明一个对象用于存储请求个数
function startLoading(targetdq) {
    loading = ElLoading.service({
        lock: true,
        text: '努力加载中...',
        background: 'rgba(0, 0, 0, 0.7)',
        target: document.querySelector(targetdq), // 设置加载动画区域
    })
}

function endLoading() {
    loading.close()
}
export function showFullScreenLoading(targetdq) {
    if (needLoadingRequestCount === 0) {
        startLoading(targetdq)
    }
    needLoadingRequestCount++
}
export function hideFullScreenLoading() {
    if (needLoadingRequestCount <= 0) return
    needLoadingRequestCount--
    if (needLoadingRequestCount === 0) {
        endLoading()
    }
}

export default {
    showFullScreenLoading,
    hideFullScreenLoading
}