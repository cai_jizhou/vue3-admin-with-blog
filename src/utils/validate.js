/*
 * @Description  : 验证
 * @Author       : CaiJiZhou
 * @Date         : 2021-11-22 09:33:21
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-23 15:36:00
 * @FilePath     : \\vue3-admin-with-blog\\src\\utils\\validate.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */

const checkEmail = (rule, value, cb) => {
    const regEmail = /^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/;
    if (regEmail.test(value)) {
        return cb();
    }
    // 返回错误提示
    cb(new Error("请输入正确的邮箱"));
};

const checkPhone = (rule, value, callback) => {
    const reg = /^[1][3,4,5,7,8][0-9]{9}$/;
    if (value == '' || value == undefined || value == null) {
        callback();
    } else {
        if ((!reg.test(value)) && value != '') {
            callback(new Error('请输入正确的电话号码'));
        } else {
            callback();
        }
    }
}

export {
    checkEmail,
    checkPhone
}