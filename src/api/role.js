/*
 * @Description  : 
 * @Author       : CaiJiZhou
 * @Date         : 2021-12-09 10:57:18
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-13 16:24:33
 * @FilePath     : \\vue3-admin-with-blog\\src\\api\\role.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */
import request from '../utils/request.js'

export const getPermissionList =async () => {
    return await request({
        method: "get",
        url: "/users/getPermissionList"
    })
}


export const getRoleList = (data) => {
    return request({
        method: "post",
        url: "/role/list",
        data,
    })
}
export const addRole = (data) => {
    return request({
        method: "post",
        url: "/role/operate/add",
        data,
    })
}
export const editRole = (data) => {
    return request({
        method: "post",
        url: "/role/operate/edit",
        data,
    })
}
export const delRole = (data) => {
    return request({
        method: "delete",
        url: "/role/operate/delete",
        data,
    })
}
