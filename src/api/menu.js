/*
 * @Author: your name
 * @Date: 2021-11-25 22:50:48
 * @LastEditTime : 2021-11-29 09:48:22
 * @LastEditors  : Shaquille.Cai
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath     : \\vue3-admin-with-blog\\src\\api\\menu.js
 */
import request from '../utils/request.js'


export const getMenuList = (data) => {
    return request({
        method: "post",
        url: "/menu/list",
        data,
        mock: false
    })
}

export const menuSubmit = (data) => {
    return request({
        url: '/menu/operate',
        method: 'post',
        data,
        mock: false
    })
}


