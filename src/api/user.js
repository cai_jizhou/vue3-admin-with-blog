/*
 * @Author: 蔡继周
 * @Date: 2021-11-13 14:00:11
 * @LastEditTime : 2021-12-29 16:15:05
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\api\\user.js
 */

import request from '../utils/request.js'

export const login = (data) => {
    return request({
        method: "post",
        url: "/users/login",
        data,
        // loadDom: '#demo'
    })
}
export const getUserInfo = () => {
    return request({
        method: "get",
        url: "/users/userInfo"
    })
}


export const getUserList = (data) => {
    return request({
        method: "post",
        url: "/users/queryUserList",
        data
    })
}
export const getAllUserList = () => {
    return request({
        method: "get",
        url: "/users/allList"
    })
}

export const delUser = (data) => {
    return request({
        method: "delete",
        url: "/users/operate/delete",
        data
    })
}

export const addUser = (data) => {
    return request({
        method: "post",
        url: "/users/operate/add",
        data
    })
}

export const updateUser = (data) => {
    return request({
        method: "post",
        url: "/users/operate/update",
        data
    })
}

export const demo = () => {
    return request({
        method: "get",
        url: "/demo/demo"
    })
}