/*
 * @Author: 蔡继周
 * @Date: 2021-11-13 14:00:11
 * @LastEditTime : 2021-12-29 16:37:49
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\api\\demo.js
 */

import request from '../utils/request.js'

export const getDemoList = (params) => {
    return request({
        method: "get",
        url: "/demo/list",
        params
    })
}

export const getAllDemoList = (params) => {
    return request({
        method: "get",
        url: "/demo/AllList",
        params
    })
}

export const addDemo = (data) => {
    return request({
        method: "post",
        url: "/demo/operate/add",
        data
    })
}

export const deleteDemo = (params) => {
    return request({
        method: "delete",
        url: "/demo/operate/del",
        params
    })
}

export const upDateDemo = (data) => {
    return request({
        method: "post",
        url: "/demo/operate/update",
        data
    })
}
