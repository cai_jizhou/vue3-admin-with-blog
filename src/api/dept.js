/*
 * @Description  : 
 * @Author       : CaiJiZhou
 * @Date         : 2021-12-07 13:30:48
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2021-12-09 09:52:47
 * @FilePath     : \\vue3-admin-with-blog\\src\\api\\dept.js
 * Copyright (C) 2021 Shaquille.Cai. All rights reserved.
 */
import request from '../utils/request.js'


export const getDeptList = (params) => {
    return request({
        method: "get",
        url: "/dept/list",
        params
    })
}

export const getAllDeptList = (params) => {
    return request({
        method: "get",
        url: "/dept/AllList",
        params
    })
}

export const addDept = (data) => {
    return request({
        method: "post",
        url: "/dept/operate/add",
        data
    })
}
export const deleteDept = (params) => {
    return request({
        method: "delete",
        url: "/dept/operate/del",
        params
    })
}

export const upDateDept = (data) => {
    return request({
        method: "post",
        url: "/dept/operate/update",
        data
    })
}

