/*
 * @Author: 蔡继周
 * @Date: 2021-11-13 14:49:42
 * @LastEditTime : 2022-01-10 22:21:34
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\myBlog\\blogAdmin\\src\\api\\index.js
 */
import * as user from './user'
import * as menu from './menu'
import * as dept from './dept'
import * as role from './role'
import * as demo from './demo'
import * as upload from './upload'


export default {
    ...menu,
    ...dept,
    ...user,
    ...role,
    ...demo,
    ...upload
}