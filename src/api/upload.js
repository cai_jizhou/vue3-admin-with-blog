/*
 * @Description  : 
 * @Author       : CaiJiZhou
 * @Date         : 2022-01-10 22:19:07
 * @LastEditors  : Shaquille.Cai
 * @LastEditTime : 2022-01-10 22:21:12
 * @FilePath     : \\myBlog\\blogAdmin\\src\\api\\upload.js
 * Copyright (C) 2022 Shaquille.Cai. All rights reserved.
 */

import request from '../utils/request.js'

export const upload = (data) => {
    return request({
        method: "post",
        url: "/file/upload",
        data,
    })
}
