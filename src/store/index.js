/*
 * @Author: 蔡继周
 * @Date: 2021-11-13 14:26:44
 * @LastEditTime : 2021-12-25 13:33:09
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\myBlog\\blogAdmin\\src\\store\\index.js
 */
import { createStore } from 'vuex'
import moduleInfo from './moduleInfo'
import tagsView from './moduleInfo/tagsView'

export default createStore({
    modules: {
        moduleInfo,
        tagsView
    }
})