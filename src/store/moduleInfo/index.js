/*
 * @Author: 蔡继周
 * @Date: 2021-11-13 14:43:14
 * @LastEditTime : 2021-12-29 14:49:33
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\store\\moduleInfo\\index.js
 */
import storage from '@/utils/storage'
import { getPermissionList } from '@/api/role'
import { login, getUserInfo } from '@/api/user'
import { FlatToTree } from "@/utils/utils";
import mainRoutes from '@/router/mainRoutes'
import store from '@/store'

const state = {
    userInfo: {},
    userStateList: [{
            value: 1,
            label: "在职",
        },
        {
            value: 2,
            label: "离职",
        },
        {
            value: 3,
            label: "试用期",
        },
    ],
    menuList: [],
    routerList:[],
    slider: {
        opened: false,
    },
};
const getters = {
    userInfo: (state) => storage.getItem("userInfo") || state.userInfo,
    opened:(state)=>state.slider.opened ,
    token: (state) => storage.getItem("token") || state.token || null,
    userStateList: (state) => state.userStateList,
    menuList: (state) => state.menuList,
    routerList: (state) => state.routerList,
};
const mutations = {
    /**
     * @description: 设置token
     * @param {*} state
     * @param {*} token
     * @return {*}
     */    
    SET_TOKEN(state, token) {
        state.token = token;
        storage.setItem('token', token)
    },
    /**
     * @description: 设置用户信息
     * @param {*} state
     * @param {*} userInfo
     * @return {*}
     */    
    SAVE_USER_INFO(state, userInfo) {
        state.userInfo = userInfo;
        storage.setItem('userInfo', userInfo)
    },
    /**
     * @description: 储存菜单列表
     * @param {*} state
     * @param {*} menuList
     * @return {*}
     */    
    SAVE_MENU_RES(state, menuList) {
        state.menuList = menuList;
    },
    /**
     * @description: 储存路由表
     * @param {*} state
     * @param {*} routerList
     * @return {*}
     */    
    SAVE_ROUTER_RES(state, routerList) {
        state.routerList = routerList;
    },
    /**
     * @description: 存储侧边栏打开或收起状态
     * @param {*} state
     * @return {*}
     */    
    TOGGLE_SLIDER(state) {
        state.slider.opened = !state.slider.opened;
    }
}

const actions = {
    
    /**
     * @description: 获取menu 抛出结果
     * @param {*} commit
     * @return {*}
     */    
    setMenuRes({ commit }) {
        return new Promise((resolve, reject) => {
            getPermissionList().then((res) => {
                const menuResult = [...res].filter((v) => v.menuType === 0 ||v.menuType ===   2); //获取菜单列表（包括目录）
                const actionResult = [...res].filter((v) => v.menuType == 1); //获取按钮权限  todo
                const menuList = FlatToTree(menuResult, "parentId", "_id", "children"); //转化树结构
                commit("SAVE_MENU_RES", menuList || []); //保存menu数据渲染layout
                //规范后台路由信息表
                const asyncRouter = [...res].filter(v => v.component)
                    .map(v => {
                    return {
                        name: v.menuNameKey,
                        path: v.path,
                        meta: {
                            title: v.menuName,
                            action:[]
                        },
                        redirect: null,
                        component: v.component,
                        parentId: v.parentId,
                        _id: v._id
                    }
                    })
                // 将按钮权限注入路由meta中,字段为‘action’
                asyncRouter.forEach(v => {
                    actionResult.forEach(z => {
                        z.parentId===v._id && (v.meta.action.push(z))
                    })
                })
                //抛出结果供 permission操作
                resolve(asyncRouter)
            });
        })
    },
    /**
     * @description: 储存路由并抛出
     * @param {*} commit
     * @param {*} routerList
     * @return {*}
     */    
    setRouterRes({commit},routerList) {
        return new Promise((resolve) => {
            commit('SAVE_ROUTER_RES', routerList)
            resolve()
        })
    },
    /**
     * @description:  登录存token
     * @param {*} commit
     * @param {*} user
     * @return {*}
     */    
    login({ commit }, user) {
        return new Promise((resolve, reject) => {
            login(user).then((res) => {
                commit("SET_TOKEN", res.token);
                resolve()
            }).catch(error => {
                reject(error)
            });
        })
    },
    /**
     * @description:获取用户信息
     * @param {*} commit
     * @return {*}
     */    
    getUserInfo({ commit }) {
        return new Promise((resolve, reject) => {
            getUserInfo().then((res) => {
                commit("SAVE_USER_INFO", res);
                resolve()
            }).catch(error => {
                reject(error)
            });
        })
    },
    /**
     * @description: 清除token，用户信息，menu表，菜单表，历史导航表
     * @param {*}
     * @return {*}
     */    
    resetToken({
        commit
    }) {
        return new Promise(resolve => {
            storage.clearAll()
            commit("SET_TOKEN", null);
            commit("SAVE_USER_INFO", null);
            commit("SAVE_MENU_RES", null);
            commit("SAVE_ROUTER_RES", null);
            store.dispatch('tagsView/delAllVisitedViews')
            //重置路由
            mainRoutes.children=mainRoutes.children.filter(v=>v.name==='Welcome')
            resolve()
        })
    },
    /**
     * @description: 收起/打开侧边栏
     * @param {*} commit
     * @return {*}
     */    
    toggle_slider({ commit }) {
        commit("TOGGLE_SLIDER");
    },
   
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}