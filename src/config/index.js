/*
 * @Author: 蔡继周
 * @Date: 2021-11-12 09:08:25
 * @LastEditTime : 2022-02-08 10:07:07
 * @LastEditors  : Shaquille.Cai
 * @Description: 环境配置封装
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\config\\index.js
 */
const env =
    import.meta.env.VITE_ENV || 'production';
const EnvConfig = {
    development: {
        baseApi: '/api',
        mockApi: 'https://www.fastmock.site/mock/339eae2aa34f1cb9bb014792ea042b5e/blog'
    },
    
    production: {
        // baseApi: 'http://www.caijizhou.online',
        baseApi: 'http://localhost:3000',
        mockApi: 'https://www.fastmock.site/mock/339eae2aa34f1cb9bb014792ea042b5e/blog'
    }
}

export default {
    env,
    mock: false,
    namespace: 'manager',
    ...EnvConfig[env]
}