/*
 * @Author: 蔡继周
 * @Date: 2021-11-14 21:55:27
 * @LastEditTime : 2022-01-10 14:09:07
 * @LastEditors  : Shaquille.Cai
 * @Description: I have to do something...
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\config\\options.js
 */
export default {
    showIndex: true, //是否显示序号
    tableHeight:600, //表格高度
    isSelection: true, //是否显示多选框
    tableSize: 1, //默认列表大小
    showRowClassName:true, //是否开启隔行变色（fixing。。。）
    defaultOptions: {
      //表格可配置项 支持所有el-table Attributes 参考elelment-Plus
      border: true, //是否带有纵向边框
      'default-expand-all':false //是否默认展开所有行
    },
    isShowToolAttr: false,   // 是否显示工具栏
    handleOption: {
      // 操作列表头名称
      handleName: "操作",
      width: 250,
      align: "center",
  }
}

//列表大小选项数组
// const sizeOption = [
//   {
//     // rowHeight: "00px",
//     // cellHeight: "20px",
//     cellPadding: "0px",
//     headerCellHeight: "40px",
//   },
//   {
//     rowHeight: "30px",
//     // cellHeight: "40px",
//     cellPadding: "8px",
//     headerCellHeight: "55px",
//   },
//   {
//     rowHeight: "60px",
//     cellHeight: "60px",
//     cellPadding: "8px",
//     headerCellHeight: "55px",
//   },
// ];