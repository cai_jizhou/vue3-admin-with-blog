/*
 * @Author: your name
 * @Date: 2021-01-08 10:11:02
 * @LastEditTime : 2021-12-28 10:41:13
 * @LastEditors  : Shaquille.Cai
 * @Description: In User Settings Edit
 * @FilePath     : \\B\\vue3-admin-with-blog\\src\\styles\\variables.scss.js
 */
const variables = {
    main_bg_color: "#001529",
    base_color: "#34acdb94",
    nav_height: "100px",
    side_close_width: "60px",
    side_open_width: "160px",
    sideBgColor: "#001529",
    sideTextColor: "#8B9095",
    sideActiveTextColor: "#fff",
    menu_active:'#34acdb94'
};
export default variables
